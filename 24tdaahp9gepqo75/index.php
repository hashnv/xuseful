<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/default.css">
    <title>xuseful</title> 
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
</head>

<body>
    <h1>xuseful is a game</h1>
    <p>this is your <b>sixth</b> clue.</p>
    <p class="clue"><?php
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';

        function generate_string($input, $strength = 16) {
            $input_length = strlen($input);
            $random_string = '';
            for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
            }

            return $random_string;
        }

        $clue_string = "tcmf07hoc2np59zg";

        for ($x = 0; $x <= 15; $x++) {
            $random_alpha = generate_string($permitted_chars, 15);
            $new_string = str_shuffle($random_alpha.$clue_string[$x]);
            echo implode(' ',str_split($new_string))."<br/>"; 
        }
    ?></p>
</body>

</html>
